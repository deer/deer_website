+++
title = "video | art | deer’s website"
template = "art.html"
insert_anchor_links = "heading"
+++

# video

You can find my videos on [the <b>Oddjobs</b> YouTube&trade;
channel](https://www.youtube.com/channel/UC-K6sM-AXGVQjVCEKgU1tGA).
