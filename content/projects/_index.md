+++
title = "projects | deer’s website"
template = "generic.html"
insert_anchor_links = "heading"

[extra]
toc = true
+++

# projects

## <b>Oddjobs</b> website

<div class="aside-wrapper"><aside>

Relevant git repository: <https://codeberg.org/oddjobs/pages>

</aside></div>

I&rsquo;ve made a website for the <b>Oddjobs</b> guild, which you can find at
[<b>https://oddjobs\.codeberg\.page/</b>](https://oddjobs.codeberg.page/).
Besides the usual guild-related stuff, the Oddjobs website also contains a
number of odd-job-related resources:

### Odd job guides

<div class="aside-wrapper"><aside>

Relevant git repository: <https://codeberg.org/oddjobs/odd_job_guides>

</aside></div>

As of yet, all of the guides on the Oddjobs website have been written by yours
truly. You can [find all of the guides
here](https://oddjobs.codeberg.page/guides/), which includes:

- [<i>Introduction To Odd
  Jobs</i>](https://oddjobs.codeberg.page/guides/introduction-to-odd-jobs/), a
  general guide &amp; introduction to the concept of &ldquo;odd jobs&rdquo;,
  particularly a gentle introduction to the most essential, primitive, &amp;
  enduring odd jobs.

  <!-- markdownlint-disable-next-line no-inline-html -->
  <span lang="es">Esta guía también ha sido traducida a Español:
  &ldquo;[Introducción A Los <i>Odd
  Jobs</i>](https://oddjobs.codeberg.page/guides/introduction-to-odd-jobs/index.spa-Latn-419.html)&rdquo;,
  gracias a <b>eriklopez95</b>.</span>
- [<i>The Divinely Blessed Knight-Errant, The Holy Physician Turned Armed
  Zealot: The STR
  Cleric</i>](https://oddjobs.codeberg.page/guides/str-cleric/), a guide to
  [STR
  clerics/priests](https://oddjobs.codeberg.page/guides/introduction-to-odd-jobs/#str-mage).
- [<i>The Martial Classicist, The Strongest Drawing Arm In All The Land: The
  Woodsman</i>](https://oddjobs.codeberg.page/guides/woodsman/), a guide to
  [woods(wo)men](https://oddjobs.codeberg.page/guides/introduction-to-odd-jobs/#woodswoman).
- [<i>Fate&rsquo;s Warlock, Fortune&rsquo;s Witch: The
  Magelet</i>](https://oddjobs.codeberg.page/guides/magelet/), a guide to
  [magelets](https://oddjobs.codeberg.page/guides/introduction-to-odd-jobs/#magelet).
- [<i>Guns To Scabbards, Swords To Holsters: The
  Swashbuckler</i>](https://oddjobs.codeberg.page/guides/swashbuckler/), a
  guide to
  [swashbucklers](https://oddjobs.codeberg.page/guides/introduction-to-odd-jobs/#swashbuckler).

### List of odd jobs

<div class="aside-wrapper"><aside>

Relevant git repository: <https://codeberg.org/oddjobs/odd_jobs>

</aside></div>

I also maintain [a short list of odd jobs on the Oddjobs
website](https://oddjobs.codeberg.page/odd-jobs.html). Do note, however, that
this list is inherently **not** exhaustive, and also has not been updated in
quite a while.

### Damage calc

<div class="aside-wrapper"><aside>

Relevant git repository:
<https://codeberg.org/oddjobs/pages/src/branch/master/dmg-calc>

</aside></div>

The Oddjobs website also features [a damage
calculator](https://oddjobs.codeberg.page/dmg-calc/) for calculating the damage
dealt by [PCs](https://en.wikipedia.org/wiki/Player_character) in pre-Big-Bang
(pre-BB) MapleStory. This calculator is intentionally designed to be highly
flexible. This highly flexible nature was originally intended to accomodate
unusual characters &amp; circumstances, for obvious reasons, but this also
makes it a great general-purpose calculator &mdash; its use is by _no means_
restricted to odd-job-related calculations.

### Gish AP calc

<div class="aside-wrapper"><aside>

Relevant git repository:
<https://codeberg.org/oddjobs/pages/src/branch/master/gish-ap-calc>

</aside></div>

The Oddjobs website also features [a &ldquo;gish <abbr title="ability point">AP</abbr>
calculator&rdquo;](https://oddjobs.codeberg.page/gish-ap-calc/), based on
original work by <b>Cortical</b> (<abbr title="in-game name">IGN</abbr> <b>GishGallop</b>) and myself. The intent
of this calculator is to help
[gishes](https://oddjobs.codeberg.page/guides/introduction-to-odd-jobs/#gish)
allocate their AP and choose their gear/buffs wisely.

The gish AP calculator operates by adding AP (into STR, INT, or LUK &mdash; but
**never** into DEX, as a heuristic), one by one, until it runs out of AP to
spend, all the while attempting to optimise for a particular value of
&ldquo;`wDominanceFactor`&rdquo;. `wDominanceFactor` is the factor by which the
gish&rsquo;s melee <abbr title="damage per minute">DPM</abbr> should exceed the gish&rsquo;s single-target magical DPM
for the selected spell. This is a [real
number](https://en.wikipedia.org/wiki/Real_number), and is typically in the
[interval] \(1,&nbsp;3\], because the gish can never attack multiple targets at
once with their melee, so they naturally tend to focus on melee for
single-target DPM and on magic for multi-target (aggregate) DPM. Thus, even a
gish who deals twice as much (`wDominanceFactor = 2`) single-target DPM with
their melee as they do with their spell, will nevertheless find their spell to
be quite useful when attacking three or more monsters at a time, and possibly
also (situationally) when attacking exactly two monsters at a time.

**Caveat emptor:** The formula for MACC (magical accuracy; the probability of
hitting a given monster with one&rsquo;s magical attacks) used by the gish AP
calculator is **known to be incorrect**. The formula used is based on work done
circa 2007, and thus _would_ be accurate to pre-BB MapleStory if it had been
done properly; however, this work was done merely to give a vague
approximation, and its methodology was questionable to begin with.
Unfortunately, we (at the time of writing) have no better approximation. This
can seriously affect the calculator&rsquo;s accuracy, as one way of
intentionally sacrificing magical damage in favour of physical damage is to
allow the probability of hitting with magic to fall below 1.

[interval]: https://en.wikipedia.org/wiki/Interval_(mathematics)

### Odd-jobbed archive

<div class="aside-wrapper"><aside>

Relevant git repository: <https://codeberg.org/oddjobs/odd-jobbed_archive>

</aside></div>

There is also [an archive of odd-job-related
things](https://oddjobs.codeberg.page/archive/) on the Oddjobs website, which
has been curated by hand.

### Unofficial odd-jobbed rankings

<div class="aside-wrapper"><aside>

Relevant git repository: <https://codeberg.org/oddjobs/odd-jobbed_rankings>

</aside></div>

I also maintain [an informal &ldquo;Unofficial &lsquo;odd-jobbed
rankings&rsquo;&rdquo;](https://oddjobs.codeberg.page/rankings/) for
MapleLegends. Note that although updating the levels and guilds on this ranking
can be automated ([see
here](https://codeberg.org/oddjobs/odd-jobbed_rankings/src/branch/master/src/main.rs)),
adding new characters to the rankings, removing characters from the rankings,
and/or changing the &ldquo;name&rdquo; (not IGN) for a character **cannot be
automated** &mdash; not even in principle.

## Vicloc resources

<div class="aside-wrapper"><aside>

Relevant git repository: <https://codeberg.org/Victoria/resources>

</aside></div>

I maintain [a repository of useful resources for
viclockers](https://codeberg.org/Victoria/resources). &ldquo;Vicloc&rdquo;
stands for &ldquo;Victoria-Island-locked&rdquo;, and is a style of play that
limits characters to Victoria Island only. This eliminates any job advancements
beyond second job, and lends itself to a fun &amp; balanced style of play
somewhat reminiscent of the earliest versions of MapleStory. For a full list of
rules, see [rules\.md in the resources
repo](https://codeberg.org/Victoria/resources/src/branch/master/rules.md).

### <i>A guide to vicloc</i>

<div class="aside-wrapper"><aside>

Relevant git repository:
<https://codeberg.org/Victoria/resources/src/branch/master/guide.md>

</aside></div>

Part of the vicloc resources includes [<i>A guide to
vicloc</i>](https://codeberg.org/Victoria/resources/src/branch/master/guide.md),
which is a wide-ranging guide that I wrote for newcomers and experienced
viclockers alike.

## scroll\_strategist

<div class="aside-wrapper"><aside>

Relevant git repositories:

- <https://codeberg.org/deer/scroll_strategist>
- <https://codeberg.org/deer/scroll_strategist_cli>

</aside></div>

I&rsquo;ve written a [Rust][rust] library called
[scroll\_strategist](https://codeberg.org/deer/scroll_strategist), and a
[CLI](https://en.wikipedia.org/wiki/Command-line_interface) to it called
[scroll\_strategist\_cli](https://codeberg.org/deer/scroll_strategist_cli),
which aim to be useful for deciding how to scroll MapleStory equipment items
(and for estimating the [market](https://en.wikipedia.org/wiki/Market_economy)
value of such equipment items). scroll\_strategist makes use of [dynamic
programming](https://en.wikipedia.org/wiki/Dynamic_programming) techniques to
find precisely optimal solutions to simple scrolling strategy problems.

For more information, see [the
README](https://codeberg.org/deer/scroll_strategist/src/branch/master/README.md).

[rust]: https://en.wikipedia.org/wiki/Rust_(programming_language)

## bulletindown

<div class="aside-wrapper"><aside>

Relevant git repository: <https://codeberg.org/deer/bulletindown>

</aside></div>

I&rsquo;ve written a [Rust][rust] program called
[bulletindown](https://codeberg.org/deer/bulletindown) that is intended to
convert [Markdown](https://en.wikipedia.org/wiki/Markdown) into
[BBCode](https://en.wikipedia.org/wiki/BBCode). I came across the need for such
a tool when crossposting [my diary](/diary) entries to the MapleLegends forums,
which uses [XenForo](https://en.wikipedia.org/wiki/XenForo) &mdash; and thus
XenForo-flavoured BBCode &mdash; for its forum posts. Additionally,
bulletindown supports
[ProBoards](https://en.wikipedia.org/wiki/ProBoards)-flavoured BBCode.

## Videogames

### <i>roll, or die trying</i>

<div class="aside-wrapper"><aside>

Relevant git repository: <https://codeberg.org/subopt/gmtk_game_jam_2022>

</aside></div>

As part of the [GMTK Game Jam 2022](https://itch.io/jam/gmtk-jam-2022), I
worked with [<b>Slime</b>](https://slimeplease.artstation.com/), <b>Tab</b>
(<b>Outside</b>), and <b>Monc</b> to [create a videogame from scratch over the
course of just 48 hours](https://en.wikipedia.org/wiki/Game_jam)! The result
was [<i>roll, or die trying</i>](https://cervid.itch.io/roll-or-die-trying), a
[platformer](https://en.wikipedia.org/wiki/Platform_game) with the unique twist
that you control a six-sided [die](https://en.wikipedia.org/wiki/Dice) in a
[2\.5D](https://en.wikipedia.org/wiki/2.5D) world where you are effectively a
square, but can roll &ldquo;away from&rdquo; or &ldquo;towards&rdquo; the
camera to change which of your six faces is visible to the camera. Rolling in
this &ldquo;third&rdquo; direction also toggles between two
&ldquo;layers&rdquo; of the map, changing which platforms you can collide (or
cannot collide) into. Each of the six faces was intended to confer different
benefits &amp; drawbacks to your mobility, although only like four or five were
actually implemented by the time that the game jam was ending.

### <i>bevy\_test</i>

<div class="aside-wrapper"><aside>

Relevant git repository: <https://codeberg.org/subopt/bevy_test>

</aside></div>

As a precursory warmup to the GMTK Game Jam 2022, to familiarise myself with
using a game engine to program a game, I made a very smol &ldquo;game&rdquo;
called [<i>bevy\_test</i>](https://subop.github.io/bevy_test/). The game
consists of some wooden crates, plus a single metal crate controlled by your
mouse that you can use to bash the wooden crates around. :P

The textures/images used in this game are due to
[<b>Slime</b>](https://slimeplease.artstation.com/).

You can also [check out the soundtrack over at the &ldquo;audio&rdquo; section
of this website](/art/audio#bevy_test).
