# deer&rsquo;s website

**<https://deer.codeberg.page/>**

Partly powered by [Zola](https://www.getzola.org/).

## building

You need all of the following in your [`$PATH`][path]:

- [`zola`](https://www.getzola.org/)
- [`rsync`](https://en.wikipedia.org/wiki/Rsync)
- [`identify`](https://en.wikipedia.org/wiki/ImageMagick)
- [`cargo`](https://rustup.rs/)
- [`tsc`](https://www.typescriptlang.org/)

Then, you can just do something like this:

```bash
git clone --recursive 'https://codeberg.org/deer/deer_website.git'
cd deer_website
cargo run --release -- --pre-zola
zola build
cargo run --release -- --post-zola
```

Output is in the `public/` directory.

[path]: https://en.wikipedia.org/wiki/PATH_(variable)
