#![forbid(unsafe_code)]
#![warn(clippy::pedantic)]

use anyhow::{anyhow, bail, Result};
use futures::future::try_join_all;
use std::{
    env,
    fs::{self, File},
    io::Write,
    path::PathBuf,
    process::{Command, Stdio},
};

#[tokio::main]
async fn main() -> Result<()> {
    let Some(arg) = env::args().nth(1) else {
        bail!("Expected either `--pre-zola` or `--post-zola` argument")
    };
    match arg.as_ref() {
        "--pre-zola" => pre_zola(),
        "--post-zola" => post_zola().await,
        _ => bail!("Expected either `--pre-zola` or `--post-zola` argument"),
    }
}

fn pre_zola() -> Result<()> {
    prepare_diary("rangifer_diary", "diary")?;
    greatest_hits()?;
    prepare_diary("rusa_toontown", "tt_diary")?;

    println!("\nCompiling TypeScript to ECMAScript...");

    let tsc_status = Command::new("tsc")
        .args(["-p", "./static/js/tsconfig.json"])
        .stdin(Stdio::null())
        .status()?;
    if !tsc_status.success() {
        bail!("`tsc` failed with exit code {:?}", tsc_status.code());
    }

    println!("\nAll done!");

    Ok(())
}

fn prepare_diary(submodule: &str, content_dir: &str) -> Result<()> {
    println!("Ensuring that the `{submodule}` submodule is initialised...");

    if let Some(entry) = fs::read_dir(format!("./{submodule}/"))?.next() {
        let _entry = entry?;
    } else {
        bail!("The `./{submodule}/` directory is empty");
    }

    println!(
        "\nCopying {submodule} content to `content/{content_dir}` with `rsync`...",
    );

    let rsync_status = Command::new("rsync")
        .args([
            "-rpth",
            "--progress",
            &format!("./{submodule}/"),
            &format!("./content/{content_dir}/"),
        ])
        .stdin(Stdio::null())
        .status()?;
    if !rsync_status.success() {
        bail!("`rsync` failed with exit code {:?}", rsync_status.code());
    }

    println!(
        "\nConverting diary entry `README.md` files to `index.md` files...",
    );

    for entry in fs::read_dir(format!("./content/{content_dir}/"))? {
        let entry = entry?;
        let mut path = entry.path();
        if entry
            .file_name()
            .to_str()
            .and_then(|s| s.parse::<u16>().ok())
            .is_some()
            && path.is_dir()
        {
            path.push(".date");
            let date = fs::read_to_string(&path)
                .map_err(|e| anyhow!("{path:?} is missing?:\n\t{e:?}"))?;
            fs::remove_file(&path)?;
            path.pop();
            path.push("README.md");
            let readme = fs::read_to_string(&path)?;
            fs::remove_file(&path)?;
            path.pop();
            path.push("index.md");
            let mut index = File::create(&path)?;
            index.write_all(b"+++\ntitle = \"")?;
            index.write_all(
                readme
                    .lines()
                    .next()
                    .ok_or_else(|| anyhow!("{path:?} is empty"))?[2..]
                    .as_bytes(),
            )?;
            index
                .write_all(b"\"\ntemplate = \"diary_entry.html\"\ndate = ")?;
            index.write_all(date.trim().as_bytes())?;
            index.write_all(b"\n+++\n\n")?;
            index.write_all(readme.as_bytes())?;
            index.flush()?;
        }
    }

    println!("\nRemoving base `README.md` and `footnotes.md`...");

    fs::remove_file(format!("./content/{content_dir}/README.md"))?;
    fs::remove_file(format!("./content/{content_dir}/footnotes.md"))?;

    println!("\nCopying `{content_dir}_index.md` to `_index.md`...");

    fs::copy(
        format!("./{content_dir}_index.md"),
        format!("./content/{content_dir}/_index.md"),
    )?;

    Ok(())
}

fn greatest_hits() -> Result<()> {
    println!("\nCopying `greatest_hits.md` to `greatest_hits/_index.md`...");

    let _ = fs::create_dir_all("./content/diary/greatest_hits");
    fs::copy(
        "./greatest_hits.md",
        "./content/diary/greatest_hits/_index.md",
    )?;

    Ok(())
}

async fn post_zola() -> Result<()> {
    try_join_all(
        fs::read_dir("./public/diary")?
            .chain(fs::read_dir("./public/tt_diary")?)
            .filter_map(|entry| {
                let Ok(entry) = entry else { return None };
                let mut path = entry.path();
                if path.is_dir() {
                    path.push("index.html");

                    Some(path)
                } else {
                    None
                }
            })
            .map(|path| tokio::spawn(munge_html(path))),
    )
    .await?;

    Ok(())
}

/// epic-level hacky garbage
async fn munge_html(path: PathBuf) -> Result<()> {
    use tokio::io::AsyncWriteExt;

    let inp = tokio::fs::read_to_string(&path).await?;
    let mut out_path = path.clone();
    out_path.set_extension(".html.temp");
    let out = tokio::fs::File::create(&out_path).await?;
    let mut out = tokio::io::BufWriter::new(out);

    let mut main_inp = None;
    for x in inp.split("<main").enumerate() {
        match x {
            (0, s) => out.write_all(s.as_bytes()).await?,
            (1, s) => main_inp = Some(s),
            _ => bail!("More than one `<main` in {}", path.display()),
        }
    }
    let Some(main_inp) = main_inp else {
        bail!("No `<main` in {}", path.display())
    };
    out.write_all(b"<main").await?;

    let mut in_tag = false;
    let mut in_img = false;
    let mut tag_buf = String::new();
    for c in main_inp.chars() {
        if in_img {
            if c == '>' {
                in_img = false;
                in_tag = false;
                let tag_buf_trimmed = tag_buf
                    .trim_end_matches(|c: char| c == '/' || c.is_whitespace());
                let formatted = if let Some((w, h)) =
                    get_inherent_res(&tag_buf).await?
                {
                    format!(
                        r#"{tag_buf_trimmed} width="{w}" height="{h}" loading="lazy" decoding="async">"#,
                    )
                } else {
                    format!(
                        r#"{tag_buf_trimmed} loading="lazy" decoding="async">"#,
                    )
                };
                out.write_all(formatted.as_bytes()).await?;
                tag_buf.truncate(0);
            } else {
                tag_buf.push(c);
            }
        } else if in_tag {
            if c.is_whitespace() && tag_buf.is_empty() {
                continue;
            }
            tag_buf.push(c);
            if c == '>' {
                in_tag = false;
                out.write_all(tag_buf.as_bytes()).await?;
                tag_buf.truncate(0);
            }
            if !c.is_ascii_alphanumeric() {
                if tag_buf.len() >= 3 && "img" == &tag_buf[..3] {
                    in_img = true;
                } else {
                    in_tag = false;
                    out.write_all(tag_buf.as_bytes()).await?;
                    tag_buf.truncate(0);
                }
            }
        } else {
            if c == '<' {
                in_tag = true;
            }
            let mut chr_buf = [0u8; 4];
            c.encode_utf8(&mut chr_buf);
            out.write_all(c.encode_utf8(&mut chr_buf).as_bytes())
                .await?;
        }
    }

    out.flush().await?;
    drop(out);
    tokio::fs::rename(out_path, path).await?;

    Ok(())
}

async fn get_inherent_res(tag_buf: &str) -> Result<Option<(usize, usize)>> {
    let Some(img_file_path) = tag_buf
        .split(r#"src="https://deer.codeberg.page/"#)
        .nth(1)
        .and_then(|s| s.split('"').next())
        .map(|s| format!("./public/{s}"))
    else {
        return Ok(None);
    };
    let id_cmd_output = String::from_utf8(
        tokio::process::Command::new("identify")
            .args(["-format", "%w %h ", &img_file_path])
            .output()
            .await?
            .stdout,
    )?;

    let mut w = None;
    let mut h = None;
    for x in id_cmd_output
        .trim_end()
        .split(' ')
        .map(str::parse)
        .enumerate()
    {
        match x {
            (0, n) => w = Some(n?),
            (1, n) => h = Some(n?),
            _ => break,
        }
    }
    let (Some(w), Some(h)) = (w, h) else {
        return Ok(None);
    };

    Ok(Some((w, h)))
}
