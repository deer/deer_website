+++
title = "rangifer’s diary | deer’s website"
description = "My personal MapleStory diary"
template = "diary.html"
insert_anchor_links = "heading"
+++

# rangifer&rsquo;s diary

I maintain a diary of my MapleStory adventures (and some other things&hellip;) originally entitled <cite>rangifer&rsquo;s diary</cite>. Although its namesake ([pugilist](https://oddjobs.codeberg.page/guides/introduction-to-odd-jobs/#pugilist) <b>rangifer</b>) is now a defunct character, it continues to retain its original name.

<aside>

**&#x2728;**&nbsp;[See here for the “greatest hits”!](/diary/greatest_hits/)

</aside>

I&rsquo;ve been writing this diary for quite a while now. You can find the entries, in reverse-chronological order, in the list below:
